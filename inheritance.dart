class person{
  String name;
  String color;
  person(String name,String color){
    this.name=name;
    this.color=color;
    print("This is from the parent class");
  }
}
  class Student extends person{
    int age;
    Student(int age):super("Sideeq","Dark"){
      this.age=age;
      print("this is fron the child class");
    }
    void output(Student me){
    print("${me.name} is ${me.color} in complexion and ${me.age} years old");
  }
  
  }
  void main(List<String> args) {
    //var some=person("Sideeq","DArk");
    var student=Student(21);
   student.output(student);
  }
