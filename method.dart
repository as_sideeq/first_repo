abstract class person{
  String name;
  int age;
  void output();
  person(String name,int age){
    this.name=name;
    this.age=age;
    print("calling a base constructor");
  }
}
class Student extends person{
String department;
Student(String department):super("Whalex",23){
  this.department=department;
}
void output(){
  print("Your name is $name , you are $age years old and you are from $department department");
}
}
 void main(List<String> args) {
   try{
     var student=Student("Computer Science");
   student.name='Sideeq';
   student.age=21;
   student.department="Telecommunication science";
   student.output();
   }
   catch(e,s){
     print("kindly check your inputs");
     print(s);
   }
   
 }